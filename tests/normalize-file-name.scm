;; Copyright (C) 2020 Liliana Marie Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(use-modules (srfi srfi-64)
             (ice-9 filesystem))

(test-begin "normalize-file-name")

(test-equal "normalize /" "/" (normalize-file-name "/"))
(test-equal "normalize /../" "/" (normalize-file-name "/../"))
(test-equal "normalize /home/../../" "/" (normalize-file-name "/home/../../"))
(test-equal "normalize .." ".." (normalize-file-name ".."))
(test-equal "normalize ./.."".." (normalize-file-name "./.."))
(test-equal "normalize foo/../.." ".." (normalize-file-name "foo/../.."))
(test-equal "normalize foo/bar"
  "foo/bar" (normalize-file-name "foo/bar"))
(test-equal "normalize foo/../foo/bar"
  "foo/bar" (normalize-file-name "foo/../foo/bar"))
(test-equal "normalize bar/../foo/bar"
  "foo/bar" (normalize-file-name "bar/../foo/bar"))
(test-equal "normalize foo/bar/baz/.."
  "foo/bar" (normalize-file-name "foo/bar/baz/.."))
(test-equal "normalize /foo/bar"
  "/foo/bar" (normalize-file-name "/foo/bar"))

(test-equal "expand single directory"
  "/homeless-shelter/.config/guile"
  (expand-file-name "guile" "/homeless-shelter/.config"))
(test-equal "expand nested directory"
  "/homeless-shelter/.local/share"
  (expand-file-name ".local/share" "/homeless-shelter"))
(test-error "expand absolute path" #t
  (expand-file-name "/gnu/store" "/homeless-shelter"))
(test-error "expand dots" #t
  (expand-file-name "../../gnu/store" "/homeless-shelter"))
(test-equal "expand sibling"
  "/home/bob"
  (expand-file-name "../bob" "/home/alice" #t))
(test-equal "path traversal deluxe"
  "/etc/hosts"
  (expand-file-name "../../../../../../etc/hosts" "/home/alice" #t))

(test-equal "relative equal absolute filename"
  "." (relative-file-name "/tmp" "/tmp"))
(test-equal "relative equal filename"
  "." (relative-file-name "foo/bar" "foo/bar"))
(test-equal "relative child"
  "baz" (relative-file-name "foo/bar/baz" "foo/bar"))
(test-equal "relative parent"
  ".." (relative-file-name "foo" "foo/bar"))
(test-error "relative mix absolute" #t
  (relative-file-name "/foo" "foo/bar"))

(test-equal "rightmost absolute none" #f
  (rightmost-absolute-file-name "."))
(test-equal "rightmost absolute root" "/"
  (rightmost-absolute-file-name "/"))
(test-equal "rightmost absolute non-root" "/tmp"
  (rightmost-absolute-file-name "/tmp"))
(test-equal "rightmost absolute nested" "/tmp"
  (rightmost-absolute-file-name "/foo/bar//tmp"))

(test-end "normalize-file-name")
