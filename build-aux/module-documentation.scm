;;;; module-documentation.scm

(define script-version "2020-10-23.19") ;UTC

;;; Copyright © 2020 Liliana Marie Prikler <liliana.prikler@gmail.com>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (ice-9 control)
             (ice-9 curried-definitions)
             (ice-9 filesystem)
             (ice-9 match)
             (ice-9 getopt-long)
             (ice-9 pretty-print)
             (srfi srfi-1)
             (srfi srfi-26)
             (texinfo)
             (texinfo reflection)
             (texinfo serialize))

(define (show-help)
  (display "Usage:
  module-documentation [OPTIONS] FILE ...

Read object documentations from the modules in FILE... and process them,
producing Texinfo output for their public interfaces.

Options:
  --template=FILE, -t FILE     Read a Texinfo template from FILE and fill
                               the docstrings into the provided anchors.
  --full, -f                   Provide a full module documentation for
                               each module.
  --output=FILE, -o FILE       Output to FILE instead of stdout.
  --help, -h                   Show this help.
  --version, -v                Show version information.
"))

(define (display-texi stexi port)
  (display (stexi->texi stexi) port))

(define (find-anchor name template)
  (find-tail
   (match-lambda
     (('anchor ('% ('name (? (cute string=? name <>)))))
      #t)
     (_ #f))
   template))

(define ((document modules full? template) port)
  (define old-fallback-path %compile-fallback-path)
  (when template
    (set! template
      (append-map
       (match-lambda
         (('para . para) (delete " " para))
         (x (list x)))
       template)))
  (dynamic-wind
    (lambda ()
      ;; we need auto-compilation to correctly resolve argument names, so
      ;; set up a temporary compile cache.
      (set! %compile-fallback-path (mkdtemp! ".cache-XXXXXX"))
      (set! %load-should-auto-compile #t))
    (lambda ()
      (let loop ((modules modules))
        (unless (null? modules)
          (let* ((m
                  (save-module-excursion
                   (lambda ()
                     (set-current-module (make-fresh-user-module))
                     (call/ec
                      (lambda (abort)
                        (primitive-load-path (car modules) abort)
                        (current-module))))))
                 (interface (module-public-interface m)))
            (when interface
              (cond
               (template
                (let ((anchor-prefix (string-join
                                      (map symbol->string
                                           (module-name interface))
                                      " ")))
                  (module-for-each
                   (lambda (sym var)
                     (let* ((anchor (string-append anchor-prefix " "
                                                   (symbol->string sym)))
                            (insert-pos (find-anchor anchor template)))
                       (when insert-pos
                         (set-cdr! insert-pos
                                   (cons
                                    (object-stexi-documentation
                                     (variable-ref var)
                                     sym)
                                    (cdr insert-pos))))))
                   interface)))
               (full?
                (display-texi
                 (module-stexi-documentation (module-name interface))
                 port))
               (else
                (module-for-each
                 (lambda (sym var)
                   (display-texi
                    (object-stexi-documentation (variable-ref var) sym)
                    port))
                 interface)))))))
      (when template
        (display (stexi->texi template) port)))
    (lambda ()
      (let ((fallback-path %compile-fallback-path))
        (set! %compile-fallback-path old-fallback-path)
        (delete-file-recursively fallback-path)))))

(define %options
  '((full     (single-char #\f) (value #f))
    (output   (single-char #\o) (value #t))
    (template (single-char #\t) (value #t))
    (help     (single-char #\h) (value #f))
    (version  (single-char #\v) (value #f))))

(define (parse-template template)
  (call-with-input-file template texi-fragment->stexi))

(let* ((opts   (getopt-long (command-line) %options))
       (option (cut option-ref opts <> <>)))
  (set! *random-state* (random-state-from-platform))
  (cond
   ((option 'help #f)    (show-help))
   ((option 'version #f) (format #t "module-documentation.scm ~A" script-version))
   (else
    (let ((file (option 'output #f))
          (document (document (option '() '()) (option 'full #f)
                              ((lambda (f) (and f (parse-template f)))
                               (option 'template #f)))))
      (if file (call-with-output-file file document)
          (document (current-output-port)))))))

;;; Local Variables:
;;; time-stamp-start: "(define script-version \""
;;; time-stamp-format: "%:y-%02m-%02d.%02H"
;;; time-stamp-time-zone: "UTC0"
;;; time-stamp-end: "\") ;UTC"
;;; End:
